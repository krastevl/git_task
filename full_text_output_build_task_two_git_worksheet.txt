$ cd src
$ make clean
rm -f *.o *.*~ *~  nmod.exe 
$ make
gcc -Wall -O3 -c _create_particle_pairs_node.c
gcc -Wall -O3 -c write_current_state.c
gcc -Wall -O3 -c export_project_from_current_state.c
gcc -Wall -O3 -c _alloc_mem_contiguous.c
gcc -Wall -O3 -c _find_meters_per_second_velocity.c
gcc -Wall -O3 -c _calculate_forces_acting_between_all_particles.c
gcc -Wall -O3 -c initialise_nbo_file.c
initialise_nbo_file.c: In function '_initialise_nbo_file':
initialise_nbo_file.c:25:26: warning: passing argument 1 to restrict-qualified parameter aliases with argument 3 [-Wrestrict]
     sprintf(local_project->outputfilename,"%s%s.nbo",local_project->outputfilename,local_project->nbo_append_text);
             ~~~~~~~~~~~~~^~~~~~~~~~~~~~~~            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
initialise_nbo_file.c:25:46: warning: '%s' directive writing up to 99 bytes into a region of size between 1 and 100 [-Wformat-overflow=]
     sprintf(local_project->outputfilename,"%s%s.nbo",local_project->outputfilename,local_project->nbo_append_text);
                                              ^~
initialise_nbo_file.c:25:5: note: 'sprintf' output between 5 and 203 bytes into a destination of size 100
     sprintf(local_project->outputfilename,"%s%s.nbo",local_project->outputfilename,local_project->nbo_append_text);
     ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
gcc -Wall -O3 -c _move_particles_RK4.c
gcc -Wall -O3 -c _delete_list_of_pairs.c
gcc -Wall -O3 -c _parse_line.c
gcc -Wall -O3 -c run_nbody_environment.c
run_nbody_environment.c: In function 'run_nbody_environment':
run_nbody_environment.c:59:60: warning: '.nmp' directive writing 4 bytes into a region of size between 1 and 100 [-Wformat-overflow=]
                 sprintf(newstring_for_exported_nmp_name,"%s.nmp",local_project->final_conditions_filename);
                                                            ^~~~
run_nbody_environment.c:59:17: note: 'sprintf' output between 5 and 104 bytes into a destination of size 100
                 sprintf(newstring_for_exported_nmp_name,"%s.nmp",local_project->final_conditions_filename);
                 ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
gcc -Wall -O3 -c _move_particles_MP.c
gcc -Wall -O3 -c _account_for_inertia.c
gcc -Wall -O3 -c main.c
main.c: In function 'main':
main.c:24:9: warning: variable 'ret_nb' set but not used [-Wunused-but-set-variable]
     int ret_nb = 0;
         ^~~~~~
gcc -Wall -O3 -c _alloc_mem.c
gcc -Wall -O3 -c _convert_gravitational_effects_into_velocity.c
gcc -Wall -O3 -c _make_set_of_particle_pairs.c
gcc -Wall -O3 -c read_single_particle.c
gcc -Wall -O3 -c free_and_delete_project.c
gcc -Wall -O3 -c load_project_file_into_nmod.c
gcc -Wall -O3 -c _clear_total_gravitational_force_accumulators.c
gcc -Wall -O3 -c _find_gravitational_interaction_between_pair_of_particles.c
gcc _create_particle_pairs_node.o write_current_state.o export_project_from_current_state.o _alloc_mem_contiguous.o _find_meters_per_second_velocity.o _calculate_forces_acting_between_all_particles.o initialise_nbo_file.o _move_particles_RK4.o _delete_list_of_pairs.o _parse_line.o run_nbody_environment.o _move_particles_MP.o _account_for_inertia.o main.o _alloc_mem.o _convert_gravitational_effects_into_velocity.o _make_set_of_particle_pairs.o read_single_particle.o free_and_delete_project.o load_project_file_into_nmod.o _clear_total_gravitational_force_accumulators.o _find_gravitational_interaction_between_pair_of_particles.o  -lm  -o nmod
Job succeeded
